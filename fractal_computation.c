/*
    fractal_computation.c contains logic for computing fractals
     and zooming to certain regions.
*/

#include "fractal_computation.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

extern video_buff_ptr video_base;
extern int demo[];


// Old fractal computation function. Very slow. Unused.
float compute_old(float _z_re, float _z_im, float _c_re, float _c_im, int n) {

  int64_t scale = 1000000;
  int64_t z_re  = (int64_t)(_z_re*100);
  int64_t z_im  = (int64_t)(_z_im*100);
  int64_t c_re  = (int64_t)(_c_re*100);
  int64_t c_im  = (int64_t)(_c_im*100);

  int64_t old_re = z_re;
  int64_t old_im = z_im;
  int64_t new_re;
  int64_t new_im;
  int64_t z;
  int8_t i;
  
  int64_t max_z = 4*scale*scale*scale;
  for (i = 1; i < n; i++) {
    //z_i+1 = z_i^2 + c = re_i^2 + 2*re_i*im_i - im_i^2
    new_re = ((old_re * old_re) - (old_im * old_im) + c_re);
    new_im = (2* old_re * old_im) + c_im;
    z = new_re * new_re + new_im * new_im;

    if ( z >= max_z){
      return i;
    }
    old_re = new_re;
    old_im = new_im;  
  }
  return n;

}

// Calculating pixel color depending on received and maximum iterations number.
uint16_t conv_color (uint8_t t, uint8_t n){

  static uint16_t val_r [256] = {0};
  static uint16_t val_g [256] = {0};
  static uint16_t val_b [256] = {0};
  static uint8_t  init  [256] = {0};

  uint16_t red   = 0;
  uint16_t green = 0;
  uint16_t blue  = 0;


  if ( ! init[t] ) {
      //serp_print("New color discovered.\n");
      init[t] = 1;
      float tmp = t;
      tmp /= n;

      val_r[t] = 9*(1-tmp)*tmp*tmp*tmp*31;
      val_g[t] = 15*(1-tmp)*(1-tmp)*tmp*tmp*63;
      val_b[t] = 8.5*(1-tmp)*(1-tmp)*(1-tmp)*tmp*31;
  }

  red = val_r[t];
  green = val_g[t];
  blue = val_b[t];

  uint16_t pixel = rgb_pack(red, green, blue);
  return pixel;
}

// Second attempt of fractal computation improving. Still slower then expected.
float compute (float _z_re, float _z_im, float _c_re, float _c_im, int n) {

    int32_t scale = 1048576;
    int32_t z_re = (int32_t)(_z_re * scale);
    int32_t z_im = (int32_t)(_z_im * scale);
    int32_t c_re = (int32_t)(_c_re * scale);
    int32_t c_im = (int32_t)(_c_im * scale);

    int32_t old_re = z_re;
    int32_t old_im = z_im;
    int32_t new_re;
    int32_t new_im;
    int32_t z;
    int8_t i;

    for (i = 1; i < n; i++) {

        int32_t tmp1;
        int32_t tmp2;

        tmp1    = old_re * old_re;
        tmp1>>=20;
        tmp2    = old_im * old_im;
        tmp2>>=20;

        new_re  = tmp1 + tmp2;
        new_re += c_re;

        new_im  = 2 * old_im * old_re;
        new_im>>=20;
        new_im += c_im;
        
        tmp1 =  new_re * new_re;
        tmp1>>=20;
        tmp2 = (new_im * new_im);
        tmp2>>=20;

        z = tmp1 + tmp2;

        if (z >= 4 * scale) {
            break;
        }
        old_re = new_re;
        old_im = new_im;
    }
    return (float)i/(float)n;
}

// Final and used fractal computation function.
// (This algorithm output was initially tested in Python)
float compute_python(float _z_re, float _z_im, float _c_re, float _c_im, int n){

    int scale = 1000;
    int64_t z_re  = (int64_t)(_z_re*scale);
    int64_t z_im  = (int64_t)(_z_im*scale);
    int64_t c_re  = (int64_t)(_c_re*scale);
    int64_t c_im  = (int64_t)(_c_im*scale);

    int64_t old_re = z_re;
    int64_t old_im = z_im;
    int64_t new_re, new_im, z;

    int64_t max_z = 4*100000000000ULL;
    int64_t tmp;

    for(int i = 0; i < n; i++){
        tmp = ((old_re * old_re) - (old_im * old_im) + c_re);
        tmp>>=4;
        new_re = tmp;
        tmp = (2* old_re * old_im);
        tmp>>=4;
        new_im = tmp + c_im;
        tmp = (new_re * new_re + new_im * new_im);
        tmp>>=4;
        z = tmp;

        if ( z >= max_z){
            return i;
        }
        tmp = new_re;
        tmp>>=5;
        old_re = tmp;
        tmp = new_im;
        tmp>>=5;
        old_im = tmp;
    }
    return n;
}

// Main fractal computation function.
void fractal(float X1, float Y1, float X2, float Y2, float C_x, float C_y, int n, int width, int height){

  unsigned char  *mem_base  = (unsigned char *)SPILED_REG_BASE;
  uint32_t rgb_knobs_value = *(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
  uint32_t blue_knob_val = rgb_knobs_value  & 0x000000FF;
  uint32_t green_knob_val = rgb_knobs_value  & 0x000FF00;

  uint32_t leds = 0;
  *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = leds;
  int part = width*height/32;
  int count = 0;
  
  int old_blue_val = blue_knob_val;
  int old_green_val = green_knob_val>>=8;

  int blue_val;
  int green_val;

  serp_print("Computing...\n");
  float t;

  float coord_x[width];
  float coord_y[height];
  float step_x;
  float step_y;


  // Calculating each pixel coordinates. (real and imaginary)
  step_y = (Y2 - Y1) / height;
  coord_y[0] = Y1;
  for (int i = 1; i< height; i++){
    coord_y[i] = coord_y[i-1] + step_y;
  }
  step_x = (X2 - X1) / width;
  coord_x[0] = X1;
  for (int i = 1; i< width; i++){
    coord_x[i] = coord_x[i-1] + step_x;
  }

  // Iterative per-pixel color calculation.
  for (int i = 0; i< height; i++){
    for (int j = 0; j< width; j++){
      rgb_knobs_value = *(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
      green_knob_val = rgb_knobs_value  & 0x000FF00;
      green_val = green_knob_val>>=8;
      
      // Naive interrupt implementation.
      if(old_green_val != green_val){
        serp_print("Going back to menu\n");
        return;
      }
      
      t = compute_python(coord_x[j], coord_y[i], C_x, C_y, n);
      (*video_base)[i][j] = conv_color (t, n);

      // LED line indicates state of calculation. (Progress bar)
      if(j + i*width > count){
        count = count + part;
        leds >>= 1;
        leds |= 1 <<31;
        *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = leds;
      }
    }
  }
  serp_print("Computation done!\n");

  rgb_knobs_value = *(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
  blue_knob_val = rgb_knobs_value  & 0x000000FF;
  green_knob_val = rgb_knobs_value  & 0x000FF00;

  old_blue_val = blue_knob_val;
  old_green_val = green_knob_val>>=8;

  // Waiting for zoom option or returning to menu.
  while(1){
    uint32_t rgb_knobs_value = *(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);

    blue_knob_val = rgb_knobs_value  & 0x000000FF;
    green_knob_val = rgb_knobs_value  & 0x000FF00;
    blue_val = blue_knob_val;
    green_val = green_knob_val>>=8;

    if(blue_val != old_blue_val){
      *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = 0;
      serp_print("Going to zoom\n");
      zoom(X1, Y1, X2, Y2, C_x, C_y, n, width, height);
      break;
    }
    if(green_val != old_green_val){
      serp_print("Going to menu\n");
      *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = 0;
      break;
    }
  }
}

// Separating calculated fractal image to four sectors for zooming.
void selection_reset( int half_x, int half_y, int width, int height, uint16_t red){
  draw_empty_rect(0,0,width-1-half_x,height-1 - half_y, red);
  draw_empty_rect(half_x,0,width-1,height-1 - half_y, red);
  draw_empty_rect(0,half_y,width-1-half_x,height-1 , red);
  draw_empty_rect(half_x,half_y,width-1,height-1, red);
}

// Fractal zooming function.
void zoom(float X1, float Y1, float X2, float Y2, float C_x, float C_y, int n, int width, int height){
  int half_x = width/2;
  int half_y = height/2;
  uint16_t red = rgb_pack(31,0,0);
  uint16_t green = rgb_pack(0,63,0);

  unsigned char  *mem_base  = (unsigned char *)SPILED_REG_BASE;
  uint32_t rgb_knobs_value;
  uint32_t red_knob_val;
  uint32_t green_knob_val;
  uint32_t blue_knob_val;

  int red_val;
  int green_val;
  int blue_val;
  int old_blue_val;
  int old_green_val;

  int red_stage;
  int old_red_stage;
  
  rgb_knobs_value = *(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
  old_blue_val = rgb_knobs_value  & 0x000000FF;
  green_knob_val = rgb_knobs_value  & 0x0000FF00;
  old_red_stage = 0;
  old_green_val = green_knob_val>>=8;

  selection_reset(half_x, half_y, width, height, red);
  
  while (1){
    rgb_knobs_value = *(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
    red_knob_val = rgb_knobs_value   & 0x00FF0000;
    green_knob_val = rgb_knobs_value  & 0x0000FF00;
    blue_knob_val = rgb_knobs_value  & 0x000000FF;
    red_val = red_knob_val>>=16;
    green_val = green_knob_val>>=8;
    blue_val = blue_knob_val;

    red_stage = (red_val % 4);
    if (old_red_stage != red_stage){
        selection_reset(half_x, half_y, width, height, red);
    }
    switch(red_stage){
      case 0:
        draw_empty_rect(0,0,width-1-half_x,height-1 - half_y, green);
      break;
      case 1:
        draw_empty_rect(half_x,0,width-1,height-1 - half_y, green);
      break;
      case 2:
        draw_empty_rect(0,half_y,width-1-half_x,height-1 , green);
      break;
      case 3:
        draw_empty_rect(half_x,half_y,width-1,height-1, green);
      break;
      }
    
    old_red_stage = red_stage;
    
    if(old_blue_val != blue_val){

      float d_X = (X2-X1)/2;
      float d_Y = (Y2-Y1)/2;
      serp_print("Zooming!\n");

      switch(red_stage){
        case 0:
          fractal (X1, Y1, X2 - d_X, Y2 - d_Y, C_x, C_y, n, width, height);
        break;
        case 1:
          fractal (X1 + d_X, Y1, X2, Y2 - d_Y, C_x, C_y, n, width, height);
        break;
        case 2:
          fractal (X1, Y1 + d_Y, X2 - d_X, Y2, C_x, C_y, n, width, height);
          
        break;
        case 3:
          fractal (X1 + d_X, Y1 + d_Y, X2, Y2, C_x, C_y, n, width, height);
        break;
        }       
      break;
      }
    
    if(old_green_val != green_val){
      serp_print("Going to menu\n");
      break;
      }
    }
 }