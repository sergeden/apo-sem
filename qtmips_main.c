/*******************************************************************
          Fractal calculator - APO semestral
          made by Denys Sergeyuk, Tom Pastuszek
          sergeden@fel.cvut.cz, pastuto@fel.cvut.cz
                            2021
 *******************************************************************/

#define _POSIX_C_SOURCE 200112L

#include "display.h"
#include "fractal_computation.h"
#include "math.h"

unsigned char *mem_base = (unsigned char *)SPILED_REG_BASE;
video_buff_ptr video_base = (video_buff_ptr)LCD_FB_START;


// Menu and input boxes basic coordinates. 
int box_X1 = 100;
int box_Y1 = 40;
int box_X2 = 380;
int box_Y2 = 285;

/*
    Internal variables for menu state monitoring.
 */
int colored_box_idx = 0;
volatile int updated_values[11] = {0};
uint32_t rgb_knobs_value;
uint32_t red_knob_val = 0;
uint32_t green_knob_val = 0;
uint32_t blue_knob_val = 0;
uint32_t old_red_knob_val = 256;
uint32_t old_green_knob_val = 256;
uint32_t old_blue_knob_val = 256;

/*
    Send single byte by UART port to a terminal, wait for ready
    to send first.
 */
void serp_tx_byte(int data) {
  while (!(serp_read_reg(SERIAL_PORT_BASE, SERP_TX_ST_REG_o) &
           SERP_TX_ST_REG_READY_m))
    ;
  serp_write_reg(SERIAL_PORT_BASE, SERP_TX_DATA_REG_o, data);
}

/*
    Write 32-bit hexadecimal number to the terminal.
 */
void serp_send_hex(unsigned int val) {
  int i;
  for (i = 8; i > 0; i--) {
    char c = (val >> 28) & 0xf;
    if (c < 10)
      c += '0';
    else
      c += 'A' - 10;
    serp_tx_byte(c);
    val <<= 4;
  }
}

/*
 * Draw menu body, title and shadow.
 */
void draw_menu() {
  uint16_t tmp_color = rgb_pack(2, 5, 2);

  // SHADOW
  shadow_rect(box_X2 - 20, box_Y1 - 10, box_X2 - 10, box_Y1 + 10, tmp_color);
  shadow_rect(box_X1 + 10, box_Y2, box_X2 + 10, box_Y2 + 10, tmp_color);
  shadow_rect(box_X2, box_Y1 + 10, box_X2 + 10, box_Y2, tmp_color);

  tmp_color = rgb_pack(20, 40, 20);
  // MENU
  draw_filled_rect(box_X1 + 20, box_Y1 - 20, box_X2 - 20, box_Y1, tmp_color);
  draw_filled_rect(box_X1, box_Y1, box_X2, box_Y2, tmp_color);

  // Title
  tmp_color = rgb_pack(31, 0, 0);
  print("--MENU--", box_X1 + 80, box_Y1 - 15, 3, tmp_color);
}

/*
    Draw menu input boxes and their labels.
 */
void draw_boxes_and_labels() {
  uint16_t tmp_color = rgb_pack(17, 35, 17);

  draw_filled_rect(box_X1 + 40, box_Y1 + 10, box_X1 + 135, box_Y1 + 40,
                   tmp_color); // X1
  draw_filled_rect(box_X1 + 180, box_Y1 + 10, box_X1 + 275, box_Y1 + 40,
                   tmp_color); // Y1
  draw_filled_rect(box_X1 + 40, box_Y1 + 60, box_X1 + 135, box_Y1 + 90,
                   tmp_color); // X2
  draw_filled_rect(box_X1 + 180, box_Y1 + 60, box_X1 + 275, box_Y1 + 90,
                   tmp_color); // Y2
  draw_filled_rect(box_X1 + 40, box_Y1 + 110, box_X1 + 135, box_Y1 + 140,
                   tmp_color); // C1
  draw_filled_rect(box_X1 + 180, box_Y1 + 110, box_X1 + 275, box_Y1 + 140,
                   tmp_color); // C2
  draw_filled_rect(box_X1 + 40, box_Y1 + 160, box_X1 + 95, box_Y1 + 190,
                   tmp_color); // n
  draw_filled_rect(box_X1 + 180, box_Y1 + 160, box_X1 + 275, box_Y1 + 190,
                   tmp_color); // step
  draw_filled_rect(box_X1 + 100, box_Y1 + 205, box_X1 + 110, box_Y1 + 215,
                   tmp_color); // demo
  draw_filled_rect(box_X1 + 100, box_Y1 + 230, box_X1 + 110, box_Y1 + 240,
                   tmp_color); // small
  draw_filled_rect(box_X1 + 180, box_Y1 + 200, box_X1 + 275, box_Y1 + 230,
                   tmp_color); // Start

  tmp_color = rgb_pack(0, 0, 0);
  // X1:
  print("X1:", box_X1 + 5, box_Y1 + 20, 3, tmp_color);

  // Y1:
  print("Y1:", box_X1 + 145, box_Y1 + 20, 3, tmp_color);

  // X2:
  print("X2:", box_X1 + 5, box_Y1 + 70, 3, tmp_color);

  // Y2:
  print("Y2:", box_X1 + 145, box_Y1 + 70, 3, tmp_color);

  // C1:
  print("C1:", box_X1 + 5, box_Y1 + 120, 3, tmp_color);

  // C2:
  print("C2:", box_X1 + 145, box_Y1 + 120, 3, tmp_color);

  // n:
  print("n:", box_X1 + 15, box_Y1 + 170, 3, tmp_color);

  // step:
  print("step:", box_X1 + 110, box_Y1 + 170, 3, tmp_color);

  // Demo:
  print("Demo:", box_X1 + 15, box_Y1 + 200, 3, tmp_color);

  // Small:
  print("Small:", box_X1 + 15, box_Y1 + 225, 3, tmp_color);

  // START
  print("START", box_X1 + 195, box_Y1 + 207, 3, rgb_pack(10, 20, 10));
}

/*
    This function draws unfilled colored rectangle 
    to highlight certain input box (given by idx).
 */
void mark_box_by_idx(int idx, uint16_t color) {
  if (idx == 0) {
    draw_empty_rect(box_X1 + 40, box_Y1 + 10, box_X1 + 135, box_Y1 + 40, color);
  } else if (idx == 1) {
    draw_empty_rect(box_X1 + 180, box_Y1 + 10, box_X1 + 275, box_Y1 + 40,
                    color);
  } else if (idx == 2) {
    draw_empty_rect(box_X1 + 40, box_Y1 + 60, box_X1 + 135, box_Y1 + 90, color);
  } else if (idx == 3) {
    draw_empty_rect(box_X1 + 180, box_Y1 + 60, box_X1 + 275, box_Y1 + 90,
                    color);
  } else if (idx == 4) {
    draw_empty_rect(box_X1 + 40, box_Y1 + 110, box_X1 + 135, box_Y1 + 140,
                    color);
  } else if (idx == 5) {
    draw_empty_rect(box_X1 + 180, box_Y1 + 110, box_X1 + 275, box_Y1 + 140,
                    color);
  } else if (idx == 6) {
    draw_empty_rect(box_X1 + 40, box_Y1 + 160, box_X1 + 95, box_Y1 + 190,
                    color);
  } else if (idx == 7) {
    draw_empty_rect(box_X1 + 180, box_Y1 + 160, box_X1 + 275, box_Y1 + 190,
                    color);
  } else if (idx == 8) {
    draw_empty_rect(box_X1 + 100, box_Y1 + 205, box_X1 + 110, box_Y1 + 215,
                    color);
  } else if (idx == 9) {
    draw_empty_rect(box_X1 + 100, box_Y1 + 230, box_X1 + 110, box_Y1 + 240,
                    color);
  } else if (idx == 10) {
    draw_empty_rect(box_X1 + 180, box_Y1 + 200, box_X1 + 275, box_Y1 + 230,
                    color);
  }
}

/*
    Function for swapping between input fields by state of red knob.
    Also highlights certain input box.
 */
void red_knob_update(uint32_t *old_red_knob_val, uint32_t *red_knob_val) {

  if (*old_red_knob_val != *red_knob_val) {

    mark_box_by_idx(colored_box_idx, rgb_pack(17, 35, 17));

    if ((*old_red_knob_val == 0) & (*red_knob_val == 255)) {
      colored_box_idx--;
    } else if((*old_red_knob_val == 255) & (*red_knob_val == 0)){
      colored_box_idx++;
    }
    else if (*old_red_knob_val < *red_knob_val) {
      colored_box_idx++;
    } else {
      colored_box_idx--;
    }
    if (colored_box_idx < 0) {
      colored_box_idx = 10;
    } else if (colored_box_idx > 10) {
      colored_box_idx = 0;
    }

    mark_box_by_idx(colored_box_idx, rgb_pack(31, 0, 0));
    *old_red_knob_val = *red_knob_val;
  }
}

int main(int argc, char *argv[]) {
  uint16_t tmp_color = rgb_pack(0, 0, 15);
  float X1_value = -2;
  float Y1_value = -2;
  float X2_value = 2;
  float Y2_value = 2;
  float C1_value = -15;
  float C2_value = 15;
  float d_value = 1.0;
  int n_value = 17;
  int is_demo = 0;
  int is_small = 0;
  int ready_to_restart = 0;

  // Main menu and background drawing routine
  serp_print("loading...\n");
  draw_filled_rect(100, 135, 400, 170,  rgb_pack(0, 0, 0));
  print("loading...", 150, 140, 4, rgb_pack(31, 63, 31));
  background_print();
  draw_menu();
  draw_boxes_and_labels();

  // Printing initialized values of fractal in menu
  tmp_color = rgb_pack(0, 63, 0);
  draw_float(X1_value,  box_X1 + 45,  box_Y1 + 20,  3, tmp_color);
  draw_float(Y1_value,  box_X1 + 185, box_Y1 + 20,  3, tmp_color);
  draw_float(X2_value,  box_X1 + 45,  box_Y1 + 70,  3, tmp_color);
  draw_float(Y2_value,  box_X1 + 185, box_Y1 + 70,  3, tmp_color);
  draw_float(C1_value,  box_X1 + 45,  box_Y1 + 120, 3, tmp_color);
  draw_float(C2_value,  box_X1 + 185, box_Y1 + 120, 3, tmp_color);
  draw_int  (n_value,   box_X1 + 45,  box_Y1 + 170, 3, tmp_color);
  draw_float(d_value,   box_X1 + 185, box_Y1 + 170, 3, tmp_color);


  // Monitoring and processing certain knobs state:
  while (1) {

    // Reading and saving each knob value.
    rgb_knobs_value =
        *(volatile uint32_t *)(mem_base + SPILED_REG_KNOBS_8BIT_o);
    red_knob_val = rgb_knobs_value & 0x00FF0000;
    blue_knob_val = rgb_knobs_value & 0x000000FF;
    green_knob_val = rgb_knobs_value & 0x0000FF00;


    // Avoiding unwanted changes by unknown init state of knobs.
    if (old_blue_knob_val == 256) {
      old_blue_knob_val = blue_knob_val;
    }
    if (old_red_knob_val == 256) {
      old_red_knob_val = red_knob_val;
    }
    if (old_green_knob_val == 256) {
      old_green_knob_val = green_knob_val;
    }

    // Restarting program on green knob change 
    // if ready_to_restart flag is set.
    if(green_knob_val != old_green_knob_val){
      if(ready_to_restart){
        main(argc, argv);
        ready_to_restart = 0;
      }
      old_green_knob_val = green_knob_val;
    }

    // Listening to red knob (if ready_to_restart flag is not set)
    if (ready_to_restart == 0) {
      red_knob_update(&old_red_knob_val, &red_knob_val);
    }

    // Listening to blue knob (if ready_to_restart flag is not set)
    // If there is any change, then update value of certain parameter, 
    // depending on whis is chosen at the moment.
    // Just updating varialbes, without redrawing them on LCD.
    if ((ready_to_restart == 0) && (old_blue_knob_val != blue_knob_val)) {
      switch (colored_box_idx) {
      case 0:
        if (blue_knob_val > old_blue_knob_val) {
          X1_value += d_value;
        } else {
          X1_value -= d_value;
        }
        updated_values[0] = 1;
        break;
      case 1:
        if (blue_knob_val > old_blue_knob_val) {
          Y1_value += d_value;
        } else {
          Y1_value -= d_value;
        }
        updated_values[1] = 1;
        break;
      case 2:
        if (blue_knob_val > old_blue_knob_val) {
          X2_value += d_value;
        } else {
          X2_value -= d_value;
        }
        updated_values[2] = 1;
        break;
      case 3:
        if (blue_knob_val > old_blue_knob_val) {
          Y2_value += d_value;
        } else {
          Y2_value -= d_value;
        }
        updated_values[3] = 1;
        break;
      case 4:
        if (blue_knob_val > old_blue_knob_val) {
          C1_value += d_value;
        } else {
          C1_value -= d_value;
        }
        updated_values[4] = 1;
        break;
      case 5:
        if (blue_knob_val > old_blue_knob_val) {
          C2_value += d_value;
        } else {
          C2_value -= d_value;
        }
        updated_values[5] = 1;
        break;
      case 6:
        if (blue_knob_val > old_blue_knob_val) {
          n_value += 1;
        } else {
          n_value -= 1;
        }
        if (n_value > 99) {
          n_value = 99;
        } else if (n_value < 0) {
          n_value = 0;
        }
        updated_values[6] = 1;
        break;
      case 7:
        if (blue_knob_val > old_blue_knob_val) {
          d_value += 0.01;
        } else {
          d_value -= 0.01;
        }
        updated_values[7] = 1;
        break;
      case 8:
        if (is_demo) {
          is_demo = 0;
        } else {
          is_demo = 1;
          if (is_small) {
            is_small = 0;
            updated_values[9] = 1;
          }
        }
        updated_values[8] = 1;
        break;
      case 9:
        if (is_small) {
          is_small = 0;
        } else {
          is_small = 1;
          if (is_demo) {
            is_demo = 0;
            updated_values[8] = 1;
          }
        }
        updated_values[9] = 1;

        break;
      case 10:
        updated_values[10] = 1;
        break;
      }

      old_blue_knob_val = blue_knob_val;

    // If there is no change on blue knob redrawing changed values.
    } else {
      uint16_t box_color = rgb_pack(17, 35, 17);
      uint16_t num_color = rgb_pack(0, 63, 0);

      // Just redrawing input boxes:

      if (updated_values[0] == 1) {
        draw_filled_rect(box_X1 + 40 + 1, box_Y1 + 10 + 1, box_X1 + 135 - 1,
                         box_Y1 + 40 - 1, box_color);
        draw_float(X1_value, box_X1 + 45, box_Y1 + 20, 3, num_color);
        updated_values[0] = 0;
      }
      if (updated_values[1] == 1) {
        draw_filled_rect(box_X1 + 180 + 1, box_Y1 + 10 + 1, box_X1 + 275 - 1,
                         box_Y1 + 40 - 1, box_color);
        draw_float(Y1_value, box_X1 + 185, box_Y1 + 20, 3, num_color);
        updated_values[1] = 0;
      }
      if (updated_values[2] == 1) {
        draw_filled_rect(box_X1 + 40 + 1, box_Y1 + 60 + 1, box_X1 + 135 - 1,
                         box_Y1 + 90 - 1, box_color);
        draw_float(X2_value, box_X1 + 45, box_Y1 + 70, 3, num_color);
        updated_values[2] = 0;
      }
      if (updated_values[3] == 1) {
        draw_filled_rect(box_X1 + 180 + 1, box_Y1 + 60 + 1, box_X1 + 275 - 1,
                         box_Y1 + 90 - 1, box_color);
        draw_float(Y2_value, box_X1 + 185, box_Y1 + 70, 3, num_color);
        updated_values[3] = 0;
      }
      if (updated_values[4] == 1) {
        draw_filled_rect(box_X1 + 40 + 1, box_Y1 + 110 + 1, box_X1 + 135 - 1,
                         box_Y1 + 140 - 1, box_color);
        draw_float(C1_value, box_X1 + 45, box_Y1 + 120, 3, num_color);
        updated_values[4] = 0;
      }
      if (updated_values[5] == 1) {
        draw_filled_rect(box_X1 + 180 + 1, box_Y1 + 110 + 1, box_X1 + 275 - 1,
                         box_Y1 + 140 - 1, box_color);
        draw_float(C2_value, box_X1 + 185, box_Y1 + 120, 3, num_color);
        updated_values[5] = 0;
      }
      if (updated_values[6] == 1) {
        draw_filled_rect(box_X1 + 40, box_Y1 + 160, box_X1 + 95, box_Y1 + 190,
                         box_color);
        draw_int(n_value, box_X1 + 45, box_Y1 + 170, 3, num_color);
        updated_values[6] = 0;
      }
      if (updated_values[7] == 1) {
        draw_filled_rect(box_X1 + 180 + 1, box_Y1 + 160 + 1, box_X1 + 275 - 1,
                         box_Y1 + 190 - 1, box_color);
        draw_float(d_value, box_X1 + 185, box_Y1 + 170, 3, num_color);
        updated_values[7] = 0;
      }
      if (updated_values[8] == 1) {
        draw_filled_rect(box_X1 + 100 + 1, box_Y1 + 205 + 1, box_X1 + 110 - 1,
                         box_Y1 + 215 - 1, box_color);
        if (is_demo) {
          serp_print("Demo mode is set.\n");
          print("x", box_X1 + 100 + 1, box_Y1 + 205 - 1, 2, rgb_pack(31, 0, 0));
        }
        updated_values[8] = 0;
      }
      if (updated_values[9] == 1) {
        draw_filled_rect(box_X1 + 100 + 1, box_Y1 + 230 + 1, box_X1 + 110 - 1,
                         box_Y1 + 240 - 1, box_color);
        if (is_small) {
          serp_print("Small mode is set.\n");
          print("x", box_X1 + 100 + 1, box_Y1 + 230 - 1, 2, rgb_pack(31, 0, 0));
        }
        updated_values[9] = 0;
      }
      
      // updated_values[10] == 1 indicates that Start button was pressed.
      // Starting certain action depending on given parameters.
      
      if (updated_values[10] == 1) {
        shadow_rect(box_X1 + 180, box_Y1 + 200, box_X1 + 275, box_Y1 + 230,
                    rgb_pack(5, 10, 5)); // shadownig Start button

        // Resetting whole display to black.
        draw_filled_rect(0, 0, LCD_WIDTH, LCD_HEIGHT, rgb_pack(0, 0, 0));

        // Show precalculated demo fractal stored in demo.c.
        // It's parameters are also printed on LCD. 
        // Estimated time to get same result by calculation is about 85 minutes.
        if (is_demo) {
          serp_print("Drawing precalculated demo fractal.\n");
          print("Precalculated demo fractal:", 30, 80, 3, rgb_pack(31, 63, 31));
          print("X1: -2", 30, 110, 3, rgb_pack(31, 63, 31));
          print("Y1: -2", 160, 110, 3, rgb_pack(31, 63, 31));
          print("X2: 2", 30, 140, 3, rgb_pack(31, 63, 31));
          print("Y2: 2", 160, 140, 3, rgb_pack(31, 63, 31));
          print("C1: -15", 30, 170, 3, rgb_pack(31, 63, 31));
          print("C2: 15", 160, 170, 3, rgb_pack(31, 63, 31));
          print("N:  17", 30, 200, 3, rgb_pack(31, 63, 31));

          demo_print();

        // Calculating fractal with resolution diminished to 50x50 pixels.
        } else if (is_small) {
          print("calculating...", 110, 140, 4, rgb_pack(31, 63, 31));
          serp_print("calculating...\n");
          fractal(X1_value, Y1_value, X2_value, Y2_value, C1_value, C2_value,
                  n_value, 50, 50);

        // Normal fractal computation on whole screen.
        } else {
          print("calculating...", 110, 140, 4, rgb_pack(31, 63, 31));
          serp_print("calculating...\n");
          fractal(X1_value, Y1_value, X2_value, Y2_value, C1_value, C2_value,
                  n_value, LCD_WIDTH, LCD_HEIGHT);
        }

        // Setting ready_to_restart flag to 1.
        // Now any change on green knob will restart main() function.
        updated_values[10] = 0;
        ready_to_restart = 1;
        serp_print("Operation is done. Ready to restart.\n");
      }
    }
  }
  return 0;
}
