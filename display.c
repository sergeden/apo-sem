/*
    display.c contains necessary functions for printing on LCD and terminal.
*/

#include "periphery.h"
#include "display.h"

extern video_buff_ptr video_base;
extern int demo[];
extern int menu[];

// dictionary[] contains sequence in which symbols
// and digits are stored in digit_gen[].
const char dictionary[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
 '-', '.', ' ', '!', '?', ':'};

// Set of all symbols that are possible to be printed.
// digits, lower, upper case alphabet amdseveral symbols.
const digit digit_gen []= {
  {   // 0
    {0, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 1, 1},
    {1, 1, 0, 1},
    {1, 0, 0, 1},
    {0, 1, 1, 0},
  },
  {   // 1
    {0, 0, 1, 0},
    {0, 1, 1, 0 },
    {0, 0, 1, 0},
    {0, 0, 1, 0},
    {0, 0, 1, 0},
    {0, 1, 1, 1},
  },
  {   // 2
    {0, 1, 1, 0},
    {1, 0, 0, 1},
    {0, 0, 0, 1},
    {0, 1, 1, 0},
    {1, 0, 0, 0},
    {1, 1, 1, 1},
  },
  {   // 3
    {0, 1, 1, 0},
    {1, 0, 0, 1},
    {0, 0, 1, 0},
    {0, 0, 0, 1},
    {1, 0, 0, 1},
    {0, 1, 1, 0},
  },
  {   // 4
    {0, 0, 1, 0},
    {0, 1, 1, 0},
    {1, 0, 1, 0},
    {1, 1, 1, 1},
    {0, 0, 1, 0},
    {0, 0, 1, 0},
  },
  {   // 5
    {1, 1, 1, 1},
    {1, 0, 0, 0},
    {1, 1, 1, 0},
    {0, 0, 0, 1},
    {1, 0, 0, 1},
    {0, 1, 1, 0},
  },
  {   // 6
    {0, 1, 1, 1},
    {1, 0, 0, 0},
    {1, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {0, 1, 1, 0},
  },
  {   // 7
    {1, 1, 1, 1},
    {0, 0, 0, 1},
    {0, 0, 1, 0},
    {0, 1, 0, 0},
    {0, 1, 0, 0},
    {0, 1, 0, 0},
  },
  {   // 8
    {0, 1, 1, 0},
    {1, 0, 0, 1},
    {0, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {0, 1, 1, 0},
  },
  {   // 9
    {0, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {0, 1, 1, 1},
    {0, 0, 0, 1},
    {0, 1, 1, 0},
  },

  {   // 'A'
    {0, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 1, 1, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
  },
  {   // 'B'
    {1, 1, 1, 1},
    {1, 0, 0, 1},
    {1, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 1, 1, 0},
  }, 
  {   // 'C'
    {0, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 0},
    {1, 0, 0, 0},
    {1, 0, 0, 1},
    {0, 1, 1, 0},
  },
  {   // 'D'
    {1, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 1, 1, 0},
  },  
  {   // 'E'
    {1, 1, 1, 1},
    {1, 0, 0, 0},
    {1, 1, 1, 0},
    {1, 0, 0, 0},
    {1, 0, 0, 0},
    {1, 1, 1, 1},
  },  
  {   // 'F'
    {1, 1, 1, 1},
    {1, 0, 0, 0},
    {1, 1, 1, 0},
    {1, 0, 0, 0},
    {1, 0, 0, 0},
    {1, 0, 0, 0},
  },  
  {   // 'G'
    {0, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 0},
    {1, 0, 1, 1},
    {1, 0, 0, 1},
    {0, 1, 1, 1},
  },  
  {   // 'H'
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 1, 1, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
  },  
  {   // 'I'
    {1, 1, 1, 0},
    {0, 1, 0, 0},
    {0, 1, 0, 0},
    {0, 1, 0, 0},
    {0, 1, 0, 0},
    {1, 1, 1, 0},
  },  
  {   // 'J'
    {0, 1, 1, 1},
    {0, 0, 0, 1},
    {0, 0, 0, 1},
    {0, 0, 0, 1},
    {1, 0, 0, 1},
    {0, 1, 1, 0},
  },  
  {   // 'K'
    {1, 0, 0, 1},
    {1, 0, 1, 0},
    {1, 1, 0, 0},
    {1, 0, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
  },  
  {   // 'L'
    {1, 0, 0, 0},
    {1, 0, 0, 0},
    {1, 0, 0, 0},
    {1, 0, 0, 0},
    {1, 0, 0, 0},
    {1, 1, 1, 1},
  },  
  {   // 'M'
    {1, 0, 0, 1},
    {1, 1, 1, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
  },  
  {   // 'N'
    {1, 0, 0, 1},
    {1, 1, 0, 1},
    {1, 0, 1, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
  },  
  {   // 'O'
    {0, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {0, 1, 1, 0},
  },  
  {   // 'P'
    {1, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 1, 1, 0},
    {1, 0, 0, 0},
    {1, 0, 0, 0},
  },  
  {   // 'Q'
    {0, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 1, 0},
    {0, 1, 0, 1},
  },  
  {   // 'R'
    {1, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
  },  
  {   // 'S'
    {0, 1, 1, 1},
    {1, 0, 0, 0},
    {0, 1, 1, 0},
    {0, 0, 0, 1},
    {0, 0, 0, 1},
    {1, 1, 1, 0},
  },  
  {   // 'T'
    {1, 1, 1, 1},
    {0, 1, 0, 0},
    {0, 1, 0, 0},
    {0, 1, 0, 0},
    {0, 1, 0, 0},
    {0, 1, 0, 0},
  },  
  {   // 'U'
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {0, 1, 1, 0},
  },  
  {   // 'V'
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {0, 1, 0, 1},
    {0, 0, 1, 0},
  },  
  {   // 'W'
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 1, 1, 1},
    {1, 0, 0, 1},
  },  
  {   // 'X'
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {0, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
  },  
  {   // 'Y'
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {0, 1, 0, 1},
    {0, 0, 1, 0},
    {0, 0, 1, 0},
    {0, 0, 1, 0},
  },  
  {   // 'Z'
    {1, 1, 1, 1},
    {0, 0, 0, 1},
    {0, 0, 1, 0},
    {0, 1, 0, 0},
    {1, 0, 0, 0},
    {1, 1, 1, 1},
  },  
  
  {   // 'a'
    {0, 0, 0, 0},
    {0, 1, 1, 0},
    {0, 0, 0, 1},
    {0, 1, 1, 1},
    {1, 0, 0, 1},
    {0, 1, 1, 1},
  },  
  {   // 'b'
    {1, 0, 0, 0},
    {1, 0, 0, 0},
    {1, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 1, 1, 0},
  },  
  {   // 'c'
    {0, 0, 0, 0},
    {0, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 0},
    {1, 0, 0, 1},
    {0, 1, 1, 0},
  },  
  {   // 'd'
    {0, 0, 0, 1},
    {0, 0, 0, 1},
    {0, 1, 1, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {0, 1, 1, 1},
  },  
  {   // 'e'
    {0, 0, 0, 0},
    {0, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 1, 1, 1},
    {1, 0, 0, 0},
    {0, 1, 1, 1},
  },  
  {   // 'f'
    {0, 0, 1, 1},
    {0, 1, 0, 0},
    {1, 1, 1, 0},
    {0, 1, 0, 0},
    {0, 1, 0, 0},
    {0, 1, 0, 0},
  },  
  {   // 'g'
    {0, 0, 0, 0},
    {0, 1, 1, 1},
    {1, 0, 0, 1},
    {0, 1, 1, 1},
    {0, 0, 0, 1},
    {1, 1, 1, 0},
  },  
  {   // 'h'
    {1, 0, 0, 0},
    {1, 0, 0, 0},
    {1, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
  },  
  {   // 'i'
    {0, 1, 0, 0},
    {0, 0, 0, 0},
    {1, 1, 0, 0},
    {0, 1, 0, 0},
    {0, 1, 0, 0},
    {1, 1, 1, 0},
  },  
  {   // 'j'
    {0, 0, 1, 0},
    {0, 0, 0, 0},
    {0, 1, 1, 0},
    {0, 0, 1, 0},
    {0, 0, 1, 0},
    {0, 1, 0, 0},
  },  
  {   // 'k'
    {1, 0, 0, 0},
    {1, 0, 0, 0},
    {1, 0, 1, 0},
    {1, 1, 0, 0},
    {1, 0, 1, 0},
    {1, 0, 1, 0},
  },  
  {   // 'l'
    {1, 1, 0, 0},
    {0, 1, 0, 0},
    {0, 1, 0, 0},
    {0, 1, 0, 0},
    {0, 1, 0, 0},
    {1, 1, 1, 0},
  },  
  {   // 'm'
    {0, 0, 0, 0},
    {0, 0, 0, 0},
    {1, 0, 0, 1},
    {1, 1, 1, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
  },  
  {   // 'n'
    {0, 0, 0, 0},
    {1, 0, 1, 0},
    {1, 1, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
  },  
  {   // 'o'
    {0, 0, 0, 0},
    {0, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {0, 1, 1, 0},
  },  
  {   // 'p'
    {0, 0, 0, 0},
    {1, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 1, 1, 0},
    {1, 0, 0, 0},
    {1, 0, 0, 0},
  },  
  {   // 'q'
    {0, 0, 0, 0},
    {0, 1, 1, 1},
    {1, 0, 0, 1},
    {0, 1, 1, 1},
    {0, 0, 0, 1},
    {0, 0, 0, 1},
  },  
  {   // 'r'
    {0, 0, 0, 0},
    {1, 0, 1, 0},
    {1, 1, 0, 1},
    {1, 0, 0, 0},
    {1, 0, 0, 0},
    {1, 0, 0, 0},
  },  
  {   // 's'
    {0, 0, 0, 0},
    {0, 1, 1, 1},
    {1, 0, 0, 0},
    {0, 1, 1, 0},
    {0, 0, 0, 1},
    {1, 1, 1, 0},
  },  
  {   // 't'
    {0, 1, 0, 0},
    {1, 1, 1, 0},
    {0, 1, 0, 0},
    {0, 1, 0, 0},
    {0, 1, 0, 0},
    {0, 0, 1, 1},
  },  
  {   // 'u'
    {0, 0, 0, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {0, 1, 1, 0},
  },  
  {   // 'v'
    {0, 0, 0, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {0, 1, 0, 1},
    {0, 0, 1, 0},
  },
  {   // 'w'
    {0, 0, 0, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {1, 1, 1, 1},
    {1, 0, 0, 1},
  },
  {   // 'x'
    {0, 0, 0, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {0, 1, 1, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
  },
  {   // 'y'
    {0, 0, 0, 0},
    {1, 0, 0, 1},
    {1, 0, 0, 1},
    {0, 1, 1, 1},
    {0, 0, 0, 1},
    {1, 1, 1, 0},
  },
  {   // 'z'
    {0, 0, 0, 0},
    {1, 1, 1, 1},
    {0, 0, 0, 1},
    {0, 0, 1, 0},
    {0, 1, 0, 0},
    {1, 1, 1, 1},
  },

  {   // '-'
    {0, 0, 0, 0},
    {0, 0, 0, 0},
    {0, 0, 0, 0},
    {0, 1, 1, 1},
    {0, 0, 0, 0},
    {0, 0, 0, 0},
  },
  {   // '.'
    {0, 0, 0, 0},
    {0, 0, 0, 0},
    {0, 0, 0, 0},
    {0, 0, 0, 0},
    {0, 0, 0, 0},
    {0, 1, 0, 0},
  },
  {   // ' '
    {0, 0, 0, 0},
    {0, 0, 0, 0},
    {0, 0, 0, 0},
    {0, 0, 0, 0},
    {0, 0, 0, 0},
    {0, 0, 0, 0},
  },
  {   // '!'
    {0, 1, 0, 0},
    {0, 1, 0, 0},
    {0, 1, 0, 0},
    {0, 1, 0, 0},
    {0, 0, 0, 0},
    {0, 1, 0, 0},
  },
  {   // '?'
    {0, 1, 0, 0},
    {1, 0, 1, 1},
    {0, 0, 1, 0},
    {0, 1, 0, 0},
    {0, 0, 0, 0},
    {0, 1, 0, 0},
  },
  {   // ':'
    {0, 0, 0, 0},
    {0, 0, 0, 0},
    {1, 0, 0, 0},
    {0, 0, 0, 0},
    {1, 0, 0, 0},
    {0, 0, 0, 0},
  },
  {   // 'unknown'
    {0, 1, 0, 1},
    {1, 0, 1, 0},
    {0, 1, 0, 1},
    {1, 0, 1, 0},
    {0, 1, 0, 1},
    {1, 0, 1, 0},
  },
}; 

// Function for packing RGB value into single uint16_t (RGB565 standart)
// (5 bits for red, 6 bits for green, 5 bits for blue)
uint16_t rgb_pack(uint16_t r, uint16_t g, uint16_t b){
  r = r&0x1F;
  g = g&0x3F;
  b = b&0x1F;

  r = r<<11;
  g = g<<5;

  uint16_t ret_val;
  ret_val = r|g|b;
  return ret_val;
}

// Get index of char contained in dictionary[].
int get_char_idx(char in){
  for(int x = 0; x < sizeof(dictionary); x++){
    if(dictionary[x] == in){
      return x;
    }
  }
  return sizeof(dictionary);
}

// Draw single char/digit on given coordinates.
// Size of digit pixel and color is required.
void draw_symbol(int inp_x, int inp_y, int digit, int pix_size, uint16_t color){
  for(  int y = 0; y < SYM_HEIGHT;   y++){
    for(int x = 0; x < SYM_WIDTH;  x++){

      int start_x = inp_x + x * pix_size;
      int start_y = inp_y + y * pix_size;
      
      if(digit_gen[digit][y][x] == 1){
        for(  int sub_y = start_y; sub_y < start_y + pix_size; sub_y++){
          for(int sub_x = start_x; sub_x < start_x + pix_size; sub_x++){
            (*video_base)[sub_y][sub_x] = color;//rgb_pack(r, g, b);
          }
        }
      }
    }
  }
}

// Printing fixed length floating-point number.
void draw_float(float num, int start_x, int start_y, int pix_size, uint16_t color){
  char str [5];
  volatile int tmp_num;
  if(num < 0){
    tmp_num = (int)(-100*num);
    print("-", start_x, start_y, pix_size, color);
    start_x += (SYM_WIDTH+1)*pix_size;
  } 
  else{
    tmp_num = (int)(100*num);
  }

  for(int i = 0; i < 5; i++){
    if(i == 2){
      str[2] = get_char_idx('.');
    }else{
      str[4-i] = tmp_num%10;
      tmp_num = tmp_num/10;
    }
  }

  for(int i = 0; i < 5; i++){
    draw_symbol(start_x, start_y, str[i], pix_size, color);
    if(str[i] == get_char_idx('.')){
      start_x += (SYM_WIDTH-1)*pix_size;
    }else{
      start_x += (SYM_WIDTH+1)*pix_size;
    }
  }
}

// Printing signed integer number. (max length is 5 digits)
void draw_int(int num, int start_x, int start_y, int pix_size, uint16_t color){
  char str [5];
  if(num < 0){
    print("-", start_x, start_y, pix_size, color);
    start_x += (SYM_WIDTH+1)*pix_size;
  } 

  for(int i = 0; i < 5; i++){
    str[4-i] = num%10;
    num = num/10;
  }

  int printing_started = 0;
  for(int i = 0; i < 5; i++){
    if(str[i] != 0){
      printing_started = 1;
    }
    if(printing_started){
      draw_symbol(start_x, start_y, str[i], pix_size, color);
      start_x += (SYM_WIDTH+1)*pix_size;
    }
  }
}

// Drawing single color filled rectangle on certain coordinates.
void draw_filled_rect(int start_x, int start_y, int end_x, int end_y, uint16_t color){

  int temp_x = end_x - start_x;
  int temp_y = end_y-start_y;
  for(int y = 0; y <= temp_y; y++){
      for(int x = 0; x <= temp_x; x++){
        (*video_base)[start_y+y][start_x+x] = color;
      }
  }
}

// Drawing single color empty rectangle on certain coordinates.
void draw_empty_rect(int start_x, int start_y, int end_x, int end_y, uint16_t color){
  int temp_x = end_x - start_x;
  int temp_y = end_y-start_y;
  for(int y = 0; y <= temp_y; y++){

      if((y == 0) || (y == temp_y)){
        for(int x = 0; x <= temp_x; x++){
          (*video_base)[start_y+y][start_x+x] = color;
        }
      } 
      else{
        (*video_base)[start_y+y][start_x] = color;
        (*video_base)[start_y+y][end_x] = color;
      }
      
  }
}

// Drawing shadow by subtracting color from each pixel.
// Beware of subtracting bigger amount than is stored (overflow).
void shadow_rect(int start_x, int start_y, int end_x, int end_y, uint16_t color){

  int temp_x = end_x - start_x;
  int temp_y = end_y-start_y;
  for(int y = 0; y <= temp_y; y++){
      for(int x = 0; x <= temp_x; x++){
        //(*video_base)[start_y+y][start_x+x] = (*video_base)[start_y+y][start_x+x] - color;
        (*video_base)[start_y+y][start_x+x] -= color;

      }
  }
}

// Printing text string on LCD display.
void print(char* text, int coord_x, int coord_y, int pix_size, uint16_t color){
  int idx = 0;

  while (*text) {
    idx = get_char_idx(*text);
    draw_symbol(coord_x, coord_y, idx, pix_size, color);
    coord_x += (SYM_WIDTH+1)*pix_size;
    text++;
  }
}

// Print main menu background image stored in demo.c
void background_print(){
  for (int i = 0; i< LCD_HEIGHT; i++){
    for (int j = 0; j< LCD_WIDTH; j++){
      (*video_base)[i][j] = menu[i*LCD_WIDTH+j];
    }
  }  
}

// Print demo fractal image stored in demo.c
void demo_print(){
  for (int i = 0; i< LCD_HEIGHT; i++){
    for (int j = 0; j< LCD_WIDTH; j++){
      (*video_base)[i][j] = demo[i*LCD_WIDTH+j];
    }
  }  
}

// Print text string to terminal.
void serp_print(char* text){
  while (*text) {
    serp_tx_byte(*text);
    text++;
  }
}