#include "stdint.h"
#include "display.h"
#include "demo.h"
#include "periphery.h"



float compute(float z_re, float z_im, float c_re, float c_im, int n);
void fractal(float X1, float Y1, float X2, float Y2, float C_x, float C_y, int n, int width, int height);
void zoom(float X1, float Y1, float X2, float Y2, float C_x, float C_y, int n, int width, int height);