#include <stdint.h>

#define SYM_WIDTH           (4)
#define SYM_HEIGHT          (6)
#define PIX_SIZE            (5)
typedef uint16_t digit[SYM_HEIGHT][SYM_WIDTH];
typedef uint8_t my_string_t [10];  

extern const digit digit_gen [];

int get_char_idx(char in);
void draw_symbol(int inp_x, int inp_y, int digit, int pix_size, uint16_t color);
void draw_float(float num, int start_x, int start_y, int pix_size, uint16_t color);
void draw_int(int num, int start_x, int start_y, int pix_size, uint16_t color);
void draw_filled_rect(int start_x, int start_y, int end_x, int end_y, uint16_t color);
void shadow_rect(int start_x, int start_y, int end_x, int end_y, uint16_t color);
void draw_empty_rect(int start_x, int start_y, int end_x, int end_y, uint16_t color);
void print(char* text, int coord_x, int coord_y, int pix_size, uint16_t color);
void serp_print(char* text);
uint16_t rgb_pack(uint16_t r, uint16_t g, uint16_t b);
void background_print();
void demo_print();
